<?php


namespace model;


abstract class Model
{
    public function setAttributesValues($data)
    {
        $model = new static;
        $modelProperties = get_object_vars($model);

        foreach ($modelProperties as $attribute => $value) {
            if (array_key_exists($attribute, $data)) {
                $model->$attribute = $data[$attribute];
            }
        }

        return $model;
    }
}