<?php


namespace model;


use interfaces\ValidatorInterfaces;

class User extends Model implements ValidatorInterfaces
{
    public $username;
    public $age;

    public function rules()
    {
        return [
            'username' => 'string',
            'age' => 'integer'
        ];
    }
}