<?php
    const DEFAULT_CONTROLLER_NAME = 'Api';
    const CONTROLLER_PATH = '\controller\\';

    $params = false;
    $method = false;

    spl_autoload_register(function ($class_name) {
        include "$class_name.php";
    });

    $uri = trim($_SERVER['REQUEST_URI'], '/');

    if ($uri !== '') {
        $uris = explode('/', $uri);
        $controller = CONTROLLER_PATH . $uris[0] . 'Controller' ?: false;

        $uris = @explode('?', $uris[1]) ?: false;

        if (array_key_exists(0, $uris)) {
            $method = $uris[0];
        }

        if (array_key_exists(1, $uris)) {
            $params = $uris[1];
        }

        if ($controller) {
            $file = $controller . '.php';
            $file = trim($file, '\\');
            if (!file_exists($file)) {
                echo 'sxal url';
                die;
            }

            $controller = new $controller();

            if ($params) {
                echo !$method ? $controller->index($params) : $controller->$method($params);
            } else {
                echo !$method ? $controller->index() : $controller->$method();
            }
        }
    } else {
        $defaultControllerName = CONTROLLER_PATH . DEFAULT_CONTROLLER_NAME . 'Controller';
        $controller = new $defaultControllerName();
        echo $controller->index();
    }

