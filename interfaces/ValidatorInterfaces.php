<?php

namespace interfaces;

interface ValidatorInterfaces
{
    public function rules();
}