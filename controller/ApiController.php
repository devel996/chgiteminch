<?php

namespace controller;
use helper\ModelHelper;
use helper\ValidatorHelper;

class ApiController {

    public function index()
    {
        echo 'index';
    }

    public function create()
    {
        if ($_POST) {
            $modelName = $_POST['model_name'];

            $model = ModelHelper::findModelByName($modelName);

            if ($model && $model = $model->setAttributesValues($_POST)) {
                $validator = new ValidatorHelper($model);
                if ($validator->validate()) {
                    header('Location: /api/success');
                } else {
                    $errors = [
                        'status' => 400,
                        'errors' => $validator->validationErrors
                    ];

                    return json_encode($errors);
                }
            }
        }
    }

    public function success()
    {
        echo 'Success!';
    }
}
