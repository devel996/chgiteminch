<?php


namespace helper;


class ModelHelper
{
    const MODEL_FOLDER = '\model\\';

    public static function findModelByName($modelName)
    {
        $modelPathWithName = self::MODEL_FOLDER . $modelName;

        if (class_exists($modelPathWithName)) {
            return new $modelPathWithName();
        }

        return false;
    }
}