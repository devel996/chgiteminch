<?php


namespace helper;


use interfaces\ValidatorInterfaces;

class ValidatorHelper
{
    public $validationErrors = [];
    public $model;

    public function __construct(ValidatorInterfaces $model)
    {
        $this->model = $model;
    }

    public function validate()
    {
        $rules = $this->model->rules();
        $modelProperties = get_object_vars($this->model);

        foreach ($modelProperties as $attribute => $value) {
            $AttributeRuleType = $rules[$attribute];
            if (!$this->$AttributeRuleType($value)) {
                $this->validationErrors[$attribute] = $this->getError($attribute, $AttributeRuleType);
            }
        }

        return empty($this->validationErrors);
    }

    private function string($value)
    {
        return is_string($value) || $value == '';
    }

    private function integer($value)
    {
        return is_numeric($value);
    }

    private function getError($attribute, $type)
    {
        return [
            'string' => "the field $attribute must be a string",
            'integer' => "The field $attribute must only contain a number"
        ][$type];
    }
}